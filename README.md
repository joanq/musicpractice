This is a little web page that generates random music sight reading exercises.

It is inspired by the exercises used at https://www.hoffmanacademy.com/ and aims
at creating a much more diverse set of exercises that the fixed set they provide
in their materials.

You can test the program at http://joanq.gitlab.io/musicpractice/
